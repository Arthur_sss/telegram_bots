import asyncio
from aiogram import Dispatcher, Bot, F
from config.config import Config, load_config
from handlers import handlers

async def main() -> None:
    config : Config = load_config('.env')
    bot = Bot(token=config.tg_bot.token)
    dp = Dispatcher()

    dp.include_router(handlers.router)

    await bot.delete_webhook(drop_pending_updates = True)       #skipping the accumulated updates
    await dp.start_polling(bot)

if __name__ == '__main__':
    asyncio.run(main())





